import { Meteor } from 'meteor/meteor';
import { Items, Memo, Time } from '../lib/collections.js';
import fs from 'fs';

const UPLOAD_DIR = '/public/upload/';

const bound = Meteor.bindEnvironment((callback) => {callback();});
const uploadPath = process.env['METEOR_SHELL_DIR'].split('/').slice(0,-3).join('/') + UPLOAD_DIR;
const loadList = function(){
    /** insert File list to DB*/
    fs.readdir(uploadPath,function(error,fileList) {
        // /** ignore hidden files */
        fileList = fileList.filter(item => !(/(^|\/)\.[^\/\.]/g).test(item));

        bound(function() {
            Items.remove({});
            fileList.forEach(function(v,i){
                bound(function(){
                    Items.insert({id:i,name:v});
                });
            });
        });
    });
};

/** StartUp Hooks*/
Meteor.startup(() => {

});

/** Items */
Meteor.publish('items.get', function(selector) {
    return Items.find(selector);
});

Meteor.publish('items.insert', function(doc) {
    Items.insert(doc);
});

Meteor.publish('items.update', function(selector,modifier) {
    Items.update(selector,modifier);
});

Meteor.publish('items.remove', function(selector) {
    Items.remove(selector);
});


/** Memo */
Meteor.publish('memo.get', function(selector) {
    return Memo.find(selector);
});

Meteor.publish('memo.insert', function(doc) {
    Memo.insert(doc);
});

Meteor.publish('memo.update', function(selector,modifier) {
    Memo.update(selector,modifier);
});

Meteor.publish('memo.remove', function(selector) {
    Memo.remove(selector);
});


/** Time */
Meteor.publish('time.get', function(selector) {
    return Time.find(selector);
});

Meteor.publish('time.insert', function(doc) {
    Time.insert(doc);
});

Meteor.publish('time.update', function(selector,modifier) {
    Time.update(selector,modifier);
});

/** File */
Meteor.methods({
    'file.upload': function (fileName, fileData) {
        fs.writeFile(uploadPath + fileName, fileData, {encoding: 'binary'}, function(e){
            console.log('File Uploaded : ' + uploadPath);

            loadList();
        });
    }
});

Meteor.methods({
    'file.loadlist': function () {
        loadList();
    }
});

Meteor.methods({
    'file.remove': function (fileName) {
        let filePath = uploadPath + fileName;
        fs.access(filePath, fs.constants.F_OK, (err) => {
            if (err) {
                return console.log('파일이 존재하지 않습니다.');
            }

            fs.unlink(filePath, (err) => err ?
                console.log(err) : console.log(`${filePath} 를 정상적으로 삭제했습니다`));
        });
    }
});