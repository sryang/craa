import { Mongo } from 'meteor/mongo';

export const Items = new Mongo.Collection('items');
export const Memo = new Mongo.Collection('memo');
export const Time = new Mongo.Collection('time');