import { Template } from 'meteor/templating';
import { Router } from 'meteor/iron:router';
import 'meteor/coloring:bootstrap5';

import { Items, Memo, Time } from '../lib/collections.js';
import moment from 'moment';

import './main.css';
import './main.html';
import './viewer.js';

const countTime = 3600;

/**
 * Router
 */
Router.route('/', function() {

    this.wait(Meteor.subscribe('time.get',{}));
    this.wait(Meteor.subscribe('items.get',{}));

    if(this.ready()) {
        var savedTime = Time.find({}).fetch();
        if(savedTime.length === 0) {
            Meteor.subscribe('time.insert',{time:countTime});
        }

        this.layout('layoutMain');
        this.render('Timer');
        this.render('List');
    } else {
        this.render('Loading');
    }

});

Router.route('/viewer', function () {

    this.layout('layoutViewer');
    this.render('Viewer', {
        data:{
            name: this.params.query.name
        }
    });
});


/**
 * Template Layout Main
 */
Template.layoutMain.onCreated(function() {
    Meteor.subscribe('memo.get',{});
    Meteor.call('file.loadlist');
});

Template.layoutMain.helpers({
    contents() {
        return Memo.findOne({});
    }
});

Template.layoutMain.events({
    'click .memoSave'(e,t) {
        let memo = Memo.findOne({});
        let target = $(e.target).attr('data-target');
        let obj  = {memo:$('#'+target).val()};

        if(memo) {
            Meteor.subscribe('memo.update', {_id:memo._id},obj);
            alert('메모가 저장되었습니다.');
        } else {
            Meteor.subscribe('memo.insert',obj);
            alert('메모가 저장되었습니다.');
        }
    },
    'click .memoDelete'(e,t) {
        if(confirm('메모를 삭제하시겠습니까?')) {
            let memo = Memo.findOne({});
            let target = $(e.target).attr('data-target');
            $('#'+target).val('');

            Meteor.subscribe('memo.remove',{_id:memo._id});
            alert('메모가 삭제되었습니다.');
        }
    },
    'keypress .memo'(e,t) {
        if(event.ctrlKey || event.metaKey) {
            switch(event.which) {
                case 83 :
                case 19 :
                    e.preventDefault();
                    alert('you pressed system key.');
                    return false;

                    break;
            }
        }
    },
});

/**
 * Template List
 */
Template.List.onCreated(function() {
    Meteor.subscribe('items.get',{});
});

Template.List.onRendered(function() {
    let list = Items.find({}).fetch();

    list.forEach(function(o,i){
        let viewerId = i+1;
        if(viewerId <= 2) {
            $('#viewer-'+ viewerId).attr('src','/viewer?name='+o.name);
        }
    });

});

Template.List.helpers({
    items() {
        return Items.find({});
    }
});

Template.List.events({
    'click .pdfLoad'(e, t) {
        let key = $(e.target).attr('data-key');
        let viewerId = $(e.target).attr('data-viewer-id');
        let item = Items.findOne({_id:key});

        $('#viewer-'+viewerId).attr('src','/viewer?name='+item.name);
    },
    'click .pdfRemove'(e, t) {
        let key = $(e.target).attr('data-key');
        let item = Items.findOne({_id:key});

        Meteor.call('file.remove', item.name ,function(){
            Meteor.subscribe('items.remove',item._id);
        });

    },
});

Template.uploadForm.events({
    'change #fileInput'(e, t){
        var file = e.currentTarget.files[0];
        var reader = new FileReader();

        reader.onload = function(fileLoadEvent) {
            Meteor.call('file.upload', file.name, reader.result);
        };
        reader.readAsBinaryString(file);
    }
});

/**
 * Template Timer
 */
Template.Timer.onRendered(function(){

    let savedTime = Time.findOne({});
    if(savedTime.time <= 0) {
        savedTime.time = countTime;
    }

    let interval = 1;
    let duration = moment.duration(savedTime.time, 'seconds');
    let display = function(dsec) {
        $('.timer').text(moment.utc(dsec).format('HH:mm:ss'));
    }

    /** initialize display */
    display(duration.asMilliseconds());

    /** set timer */
    let timer = setInterval(function () {
        duration = moment.duration(duration.asSeconds() - interval, 'seconds');
        if(duration.asSeconds() <= 0) {
            clearInterval(timer);
        }
        display(duration.asMilliseconds());

        /** save timer */
        Meteor.subscribe('time.update', {_id:savedTime._id},{time:duration.asSeconds()});

    }, interval * 1000);

});