import './viewer.css';
import './viewer.html';
import { __webpack_exports__, __webpack_require__ } from './lib.js';

const uploadPath = 'upload/';

/**
 *
 */
Template.Viewer.onRendered(function () {
    {
        const filename = this.data.name || null;
        if(!filename) {
            console.log('No exist file.');
        }

        var exports = __webpack_exports__;

        Object.defineProperty(exports, "__esModule", ({
            value: true
        }));
        Object.defineProperty(exports, "PDFViewerApplication", ({
            enumerable: true,
            get: function () {
                return _app.PDFViewerApplication;
            }
        }));
        Object.defineProperty(exports, "PDFViewerApplicationOptions", ({
            enumerable: true,
            defaultUrl: '',
            get: function () {
                return _app_options.AppOptions;
            }
        }));

        var _app_options = __webpack_require__(1);
        var _app = __webpack_require__(2);

        _app_options.AppOptions.set('defaultUrl',uploadPath + filename);
        console.log(uploadPath + filename);

        const pdfjsVersion = '2.12.313';
        const pdfjsBuild = 'a2ae56f39';
        window.PDFViewerApplication = _app.PDFViewerApplication;
        window.PDFViewerApplicationOptions = _app_options.AppOptions;

        __webpack_require__(39);
        __webpack_require__(45);

        function getViewerConfiguration() {
            let errorWrapper = {
                container: document.getElementById("errorWrapper"),
                errorMessage: document.getElementById("errorMessage"),
                closeButton: document.getElementById("errorClose"),
                errorMoreInfo: document.getElementById("errorMoreInfo"),
                moreInfoButton: document.getElementById("errorShowMore"),
                lessInfoButton: document.getElementById("errorShowLess")
            };
            return {
                appContainer: document.body,
                mainContainer: document.getElementById("viewerContainer"),
                viewerContainer: document.getElementById("viewer"),
                eventBus: null,
                toolbar: {
                    container: document.getElementById("toolbarViewer"),
                    numPages: document.getElementById("numPages"),
                    pageNumber: document.getElementById("pageNumber"),
                    customScaleOption: document.getElementById("customScaleOption"),
                    previous: document.getElementById("previous"),
                    next: document.getElementById("next"),
                    zoomIn: document.getElementById("zoomIn"),
                    zoomOut: document.getElementById("zoomOut"),
                    openFile: document.getElementById("openFile"),
                },
                sidebar: {
                    outerContainer: document.getElementById("outerContainer"),
                    viewerContainer: document.getElementById("viewerContainer"),
                    toggleButton: document.getElementById("sidebarToggle"),
                    thumbnailButton: document.getElementById("viewThumbnail"),
                    outlineButton: document.getElementById("viewOutline"),
                    attachmentsButton: document.getElementById("viewAttachments"),
                    layersButton: document.getElementById("viewLayers"),
                    thumbnailView: document.getElementById("thumbnailView"),
                    outlineView: document.getElementById("outlineView"),
                    attachmentsView: document.getElementById("attachmentsView"),
                    layersView: document.getElementById("layersView"),
                    outlineOptionsContainer: document.getElementById("outlineOptionsContainer"),
                    currentOutlineItemButton: document.getElementById("currentOutlineItem")
                },
                sidebarResizer: {
                    outerContainer: document.getElementById("outerContainer"),
                    resizer: document.getElementById("sidebarResizer")
                },
                passwordOverlay: {
                    overlayName: "passwordOverlay",
                    container: document.getElementById("passwordOverlay"),
                    label: document.getElementById("passwordText"),
                    input: document.getElementById("password"),
                    submitButton: document.getElementById("passwordSubmit"),
                    cancelButton: document.getElementById("passwordCancel")
                },
                documentProperties: {
                    overlayName: "documentPropertiesOverlay",
                    container: document.getElementById("documentPropertiesOverlay"),
                    closeButton: document.getElementById("documentPropertiesClose"),
                    fields: {
                        fileName: document.getElementById("fileNameField"),
                        fileSize: document.getElementById("fileSizeField"),
                        title: document.getElementById("titleField"),
                        author: document.getElementById("authorField"),
                        subject: document.getElementById("subjectField"),
                        keywords: document.getElementById("keywordsField"),
                        creationDate: document.getElementById("creationDateField"),
                        modificationDate: document.getElementById("modificationDateField"),
                        creator: document.getElementById("creatorField"),
                        producer: document.getElementById("producerField"),
                        version: document.getElementById("versionField"),
                        pageCount: document.getElementById("pageCountField"),
                        pageSize: document.getElementById("pageSizeField"),
                        linearized: document.getElementById("linearizedField")
                    }
                },
                errorWrapper,
                printContainer: document.getElementById("printContainer"),
                openFileInputName: "fileInput",
                debuggerScriptPath: "./debugger.js"
            };
        }


        function webViewerLoad() {
            const config = getViewerConfiguration();
            const event = document.createEvent("CustomEvent");
            event.initCustomEvent("webviewerloaded", true, true, {
                source: window
            });

            try {
                parent.document.dispatchEvent(event);
            } catch (ex) {
                console.error(`webviewerloaded: ${ex}`);
                document.dispatchEvent(event);
            }

            _app.PDFViewerApplication.run(config);
        }

        if (document.blockUnblockOnload) {
            document.blockUnblockOnload(true);
        }

        if (document.readyState === "interactive" || document.readyState === "complete") {
            webViewerLoad();
        } else {
            document.addEventListener("DOMContentLoaded", webViewerLoad, true);
        }
    }
})
;



